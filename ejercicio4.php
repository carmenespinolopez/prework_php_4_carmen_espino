<?php

//Recibe el path del archivo a leer en $in, 
function reemplazar($in, $out, $search, $replace) {
    $fdIn = fopen($in, 'r');
    //Lo abrimos con w porque queremos que lo abra en lectura, lo vacíe si tiene contenido y, en caso de no existir, crearlo.
    $fdOut = fopen($out, 'w');
    $palabra = "";

    while (($contenido = fgetc($fdIn)) !== false) { 
        if ($contenido === '.' || $contenido === ' ' || $contenido === '\n' || $contenido === '-' || $contenido === '!' || $contenido === '¡' || $contenido === '?' || $contenido === '¿' || $contenido === ':' || $contenido === ';' || $contenido === ',') {
            
            /*if ($palabra === $search) {
                $palabra = $replace . $contenido;
                fwrite($fdOut, $palabra);
            } else {
                $palabra = $palabra . $contenido;
                fwrite($fdOut, $palabra);
            }*/

            /* En caso de que la palabra coincida con la que buscamos, pasamos el contenido a $palabra de manera que siempre va
            a escribir $palabra coincida o no */
            if ($palabra === $search) {
                $palabra = $replace;
            }

            $palabra = $palabra . $contenido;
            fwrite($fdOut, $palabra);

            $palabra = "";
        } else {
            $palabra = $palabra . $contenido;
        }
    }
    
    fclose($fdIn);
    fclose($fdOut);
}

reemplazar("elquijote.txt", "elquijote2.txt", "tomar", "dejar");

?>